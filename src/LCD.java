public class LCD {

    private String status;
    private int volume;
    private int brightness;
    private String cable;

    private int cableOption = 1;


    public void turnOff() {
        this.status = "LCD mati";
    }

    public void turnOn() {
        this.status = "LCD nyala";
    }

    public void freeze() {
        this.status = "LCD freeze";
    }

    public void volumeUp(){
        this.volume += 2;
    }

    public void volumeDown(){
        this.volume -= 2;
    }

    public void setVolume(int volume){
        this.volume = volume;
    }

    public void brightnessUp(){
        this.brightness += 2;
    }

    public void brightnessDown(){
        this.brightness -= 2;
    }

    public void setBrigthness(int brightness){
        this.brightness = brightness;
    }

    public void cableUp() {
        this.cableOption++;
        LCDcable();
    }

    public void cableDown() {
        this.cableOption--;
        LCDcable();
    }

    public void LCDcable() {
        switch (cableOption) {
            case 1:
                cable = "DVI";
                break;
            case 2:
                cable = "VGA";
                break;
            case 3:
                cable = "HDMI";
                break;
            default:
                cable = "Pilihan cable tidak tersedia";
                break;
        }
    }

    public void setCable(String cable){
        this.cable = cable;
    }

    public void displayInfo() {
        System.out.println(":::::::::::::::::::: LCD :::::::::::::::::::::\n");
        System.out.println("Status LCD saat ini\t\t: "+status);
        System.out.println("Volume LCD saat ini\t\t: "+volume);
        System.out.println("Brightness LCD saat ini\t\t: "+brightness);
        System.out.println("Cable LCD saat ini\t\t: "+cable);
    }
}
