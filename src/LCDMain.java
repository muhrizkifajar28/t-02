public class LCDMain {
    public static void main(String[] args) {
        LCD lcd = new LCD();

        lcd.turnOn();

        lcd.setVolume(50);
        lcd.volumeDown();
        lcd.volumeDown();
        lcd.volumeDown();
        lcd.volumeDown();

        lcd.setBrigthness(70);
        lcd.brightnessUp();
        lcd.brightnessUp();
        lcd.brightnessUp();

        lcd.cableUp();
        lcd.cableUp();
        
        lcd.displayInfo();
    }
}
